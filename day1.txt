stages:
  - deploy
  - build
  - test
 

job1:
  script:
    - echo "Welcome to the lab7"

build-my-app:
  stage: build
  script:
    - echo "Building the Application"

test-my-app:
  stage: test
  script:
    - echo "Testing the Application"

release-my-app:
  script:
    - echo "Releasing the Application"

deploy-my-app:
  stage: deploy
  script:
    - echo "Deploying the Application"

monitor-my-app:
  script:
    - echo "Monitoring the Application"
